<?php

namespace QUAY\EncuestasCGCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Encuesta
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Encuesta
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha", type="string", length=60, nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="turno", type="string", length=60, nullable=true)
     */
    private $turno;

    /**
     * @ORM\ManyToOne(targetEntity="Tipo", inversedBy="encuestas")
     * @ORM\JoinColumn(name="tipo_id", referencedColumnName="id")
     */
    private $tipo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="primera_vez", type="boolean", nullable=true)
     */
    private $primeraVez;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pertinencia", type="string", length=60,  nullable=true)
     */
    private $pertinencia; //COMPETENCIA

    /**
     * @var boolean
     *
     * @ORM\Column(name="completitud", type="string", nullable=true)
     */
    private $completitud;

    /**
     * @var integer
     *
     * @ORM\Column(name="satisfaccion", type="string", nullable=true)
     */
    private $satisfaccion;

    /**
     * @var boolean
     *
     * @ORM\Column(name="empatia", type="string", nullable=true)
     */
    private $empatia; 

    /**
     * @var string
     *
     * @ORM\Column(name="comentario", type="string", length=250, nullable=true)
     */
    private $comentario;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo", type="integer", nullable=true)
     */
    private $codigo; 


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     *
     * @return Encuesta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set turno
     *
     * @param string $turno
     *
     * @return Encuesta
     */
    public function setTurno($turno)
    {
        $this->turno = $turno;

        return $this;
    }

    /**
     * Get turno
     *
     * @return string
     */
    public function getTurno()
    {
        return $this->turno;
    }

    /**
     * Set primeraVez
     *
     * @param boolean $primeraVez
     *
     * @return Encuesta
     */
    public function setPrimeraVez($primeraVez)
    {
        $this->primeraVez = $primeraVez;

        return $this;
    }

    /**
     * Get primeraVez
     *
     * @return boolean
     */
    public function getPrimeraVez()
    {
        return $this->primeraVez;
    }

    /**
     * Set pertinencia
     *
     * @param string $pertinencia
     *
     * @return Encuesta
     */
    public function setPertinencia($pertinencia)
    {
        $this->pertinencia = $pertinencia;

        return $this;
    }

    /**
     * Get pertinencia
     *
     * @return string
     */
    public function getPertinencia()
    {
        return $this->pertinencia;
    }

    /**
     * Set completitud
     *
     * @param string $completitud
     *
     * @return Encuesta
     */
    public function setCompletitud($completitud)
    {
        $this->completitud = $completitud;

        return $this;
    }

    /**
     * Get completitud
     *
     * @return string
     */
    public function getCompletitud()
    {
        return $this->completitud;
    }

    /**
     * Set satisfaccion
     *
     * @param string $satisfaccion
     *
     * @return Encuesta
     */
    public function setSatisfaccion($satisfaccion)
    {
        $this->satisfaccion = $satisfaccion;

        return $this;
    }

    /**
     * Get satisfaccion
     *
     * @return string
     */
    public function getSatisfaccion()
    {
        return $this->satisfaccion;
    }

    /**
     * Set empatia
     *
     * @param string $empatia
     *
     * @return Encuesta
     */
    public function setEmpatia($empatia)
    {
        $this->empatia = $empatia;

        return $this;
    }

    /**
     * Get empatia
     *
     * @return string
     */
    public function getEmpatia()
    {
        return $this->empatia;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return Encuesta
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;

        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set tipo
     *
     * @param \QUAY\EncuestasCGCBundle\Entity\Tipo $tipo
     *
     * @return Encuesta
     */
    public function setTipo(\QUAY\EncuestasCGCBundle\Entity\Tipo $tipo = null)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return \QUAY\EncuestasCGCBundle\Entity\Tipo
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set codigo
     *
     * @param integer $codigo
     *
     * @return Encuesta
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer
     */
    public function getCodigo()
    {
        return $this->codigo;
    }
}
