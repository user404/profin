<?php

namespace QUAY\EncuestasCGCBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tipo
{

    /**
    *@ORM\OneToMany(targetEntity="Encuesta", mappedBy="tipo")
    **/
    private $encuestas;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=60)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Tipo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->encuestas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add encuesta
     *
     * @param \QUAY\EncuestasCGCBundle\Entity\Encuesta $encuesta
     *
     * @return Tipo
     */
    public function addEncuesta(\QUAY\EncuestasCGCBundle\Entity\Encuesta $encuesta)
    {
        $this->encuestas[] = $encuesta;

        return $this;
    }

    /**
     * Remove encuesta
     *
     * @param \QUAY\EncuestasCGCBundle\Entity\Encuesta $encuesta
     */
    public function removeEncuesta(\QUAY\EncuestasCGCBundle\Entity\Encuesta $encuesta)
    {
        $this->encuestas->removeElement($encuesta);
    }

    /**
     * Get encuestas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEncuestas()
    {
        return $this->encuestas;
    }
}
