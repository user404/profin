<?php

namespace QUAY\EncuestasCGCBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EncuestaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'text', array( 'label'=>'Fecha:','required'=>true,'attr' => array('class'=>'form-control')))
           // ->add('turno', 'choice', array( 'choices'  => array('placeholder' => 'Seleccione una opcion','Mañana' => 'Mañana', 'Tarde' => 'Tarde', 'Noche' => 'Noche','required'=>true), 'choices_as_values' => false,'attr' => array('class'=>'form-control')))
            ->add('turno', 'choice', array('choices' => array('Mañana' => 'Mañana', 'Tarde' => 'Tarde', 'Noche' => 'Noche'),'required'=>true ,'attr' => array('class'=>'form-control')))
            ->add('tipo',"entity",array('placeholder' => 'Seleccione una opcion', 'label'=>'Tipo de Credito:','class'=>'QUAYEncuestasCGCBundle:Tipo', 'property'=>'nombre','required'=>true,'attr' => array('class'=>'form-control') ))
            ->add('primeraVez', 'checkbox',array('label'=>'Primera Vez:','required'=>false))
//            ->add('pertinencia', 'checkbox',array('label'=>'Pertinencia:','required'=>false))
            ->add('pertinencia', 'choice', array(
                    'expanded' => true,
                    'required'=>true,
            //  'multiple' => true,
                    'choices'  => array(
                        'SI' => 'SI',
                        'MEDIANAMENTE'  => 'MEDIANAMENTE',
                        'NO'   => 'NO',
                    ),
                ))

            ->add('completitud', 'choice', array(
                    'expanded' => true,
                    'required'=>true,
                    'choices'  => array(
                        'SI' => 'SI',
                        'MEDIANAMENTE'  => 'MEDIANAMENTE',
                        'NO'   => 'NO',
                    ),
                ))

            ->add('satisfaccion', 'choice', array(
                    'expanded' => true,
                    'required'=>true,
                    'choices'  => array(
                        'SI' => 'SI',
                        'MEDIANAMENTE'  => 'MEDIANAMENTE',
                        'NO'   => 'NO',
                    ),
                ))

            ->add('empatia', 'choice', array(
                    'expanded' => true,
                    'required'=>true,
                    'choices'  => array(
                        'EXCELENTE' => 'EXCELENTE',
                        'MUY BUENO'  => 'MUY BUENO',
                        'BUENO'   => 'BUENO',
                        'REGULAR'   => 'REGULAR',
                        'NECESITA MEJORAS'   => 'NECESITA MEJORAS',
                    ),
                )) 
            
            ->add('comentario','textarea', array( 'label'=>'Comentario:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('codigo', 'integer', array( 'label'=>'Codigo Operador:','required'=>false,'attr' => array('class'=>'form-control')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'QUAY\EncuestasCGCBundle\Entity\Encuesta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'quay_encuestascgcbundle_encuesta';
    }
}
