<?php

namespace QUAY\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class QUAYUserBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
}
